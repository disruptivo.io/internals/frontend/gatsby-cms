import { useEffect, useState } from 'react'

export default (project, refresh) => {
  const [files, setFiles] = useState([])
  
  useEffect(() => {
    if (project)
      Meteor
      .call(
        'getMarkdownFiles',
        project.name,
        (error, data) => setFiles(data)
      )
  }, [project, refresh])

  return [files, setFiles]
}