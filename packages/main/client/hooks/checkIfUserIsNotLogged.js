import { useEffect } from 'react'

export default ({ currentUser, history }) => {
  useEffect(() => {
    if (!currentUser)
      history.push({
        pathname: '/login'
      })
  }, [currentUser])
}