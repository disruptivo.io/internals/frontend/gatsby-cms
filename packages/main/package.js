Package.describe({
  name: 'main',
  version: '1.0.0',
  summary: 'Simple Gatsby CMS for devs',
})

Package.onUse(function(api) {
  api.use([
    'ecmascript',
    'fourseven:scss@4.12.0',
  ])

  api.addFiles('assets/css/index.scss')
  api.addAssets('assets/images/favicon.png', 'client')

  api.mainModule('client/index.js', 'client')
  api.mainModule('server/index.js', 'server')
})
