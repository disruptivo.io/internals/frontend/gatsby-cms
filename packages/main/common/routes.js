import { addRoute } from 'meteor/vulcan:core'

addRoute([
  { name: 'login', path: '/login', componentName: 'Login' },
  { name: 'enterprises', path: '/', componentName: 'Enterprises' },
  { name: 'projects', path: '/:enterpriseId/projects', componentName: 'Projects' },
  { name: 'projectDetail', path: '/projects/:projectId', componentName: 'ProjectDetail' },
])
