import React from 'react'
import { registerComponent, Components } from 'meteor/vulcan:core'

const ProjectDetailToolbar = ({ name }) => {

  return (
    <Components.Toolbar
      left={`Project: ${name}`}
      right={<Components.AccountsLoginForm/>}
    />
  )
}

registerComponent({ name: 'ProjectDetailToolbar', component: ProjectDetailToolbar })
export default ProjectDetailToolbar