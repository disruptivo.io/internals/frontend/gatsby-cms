import React from 'react'
import { registerComponent, Components } from 'meteor/vulcan:core'

const EnterpriseToolbar = () => {

  return (
    <Components.Toolbar
      float
      left={'Enterprises'}
      right={<Components.AccountsLoginForm/>}
    />
  )
}

registerComponent({ name: 'EnterpriseToolbar', component: EnterpriseToolbar })
export default EnterpriseToolbar