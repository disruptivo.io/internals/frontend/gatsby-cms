import React from 'react'
import { registerComponent, Components } from 'meteor/vulcan:core'

const ProjectToolbar = () => {

  return (
    <Components.Toolbar
      float
      left={'Projects'}
      right={<Components.AccountsLoginForm/>}
    />
  )
}

registerComponent({ name: 'ProjectToolbar', component: ProjectToolbar })
export default ProjectToolbar