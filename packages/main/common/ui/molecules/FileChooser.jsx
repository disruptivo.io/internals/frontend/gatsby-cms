import React from 'react'
import { registerComponent, Components } from 'meteor/vulcan:core'

const FileChooser = ({ path, onUpload, onUploaded, disabled, loading }) => {

  const onClick = () => {
    this.fileChooser.click()
  }

  const onChange = (event) => {
    const file = event.currentTarget.files[0]
    uploadFile(file)
  }

  const uploadFile = (file) => {
    const reader = new FileReader();
    
    reader.onload = (event) => {
      onUpload()
      Meteor
      .call(
        'uploadFile',
        { path, fileName: file.name, file: reader.result },
        (error, data) => {
          if (error) return error.message
          
          alert(data.message)
          onUploaded()
        }
      );
    }

    reader.readAsBinaryString(file);
  }

  return (
    <div
      className="d-flex justify-content-end"
      style={{
        height: '3vh'
      }}
    >
      <input
        ref={input => this.fileChooser = input}
        className="d-none"
        type='file'
        onChange={onChange}
      />
      <Components.AddFileIcon
        loading={loading}
        disabled={disabled}
        onClick={onClick}
      />
    </div>
  )
}

registerComponent({ name: 'FileChooser', component: FileChooser })
export default FileChooser