import React, { useEffect } from 'react'
import { Components, withCurrentUser, registerComponent } from 'meteor/vulcan:core'

const Login = ({ currentUser, history }) => {

  useEffect(() => {
    if (currentUser)
      history.push({ pathname: '/' })
  }, [currentUser])
  
  return (
    <Components.LoginTemplate>
      <Components.AccountsLoginForm
        showSignUpLink={false}
      />
    </Components.LoginTemplate>
  )
}

registerComponent({ name: 'Login', component: Login, hocs: [withCurrentUser] })
export default Login