import React, { useState } from 'react'
import { registerComponent, Components, withCurrentUser, useMulti2 } from 'meteor/vulcan:core'
import { UnControlled as CodeMirror } from 'react-codemirror2'
import checkIfUserIsNotLogged from '../../../client/hooks/checkIfUserIsNotLogged'
import getMarkdownFiles from '../../../client/hooks/getMarkdownFiles'
import { Treebeard } from 'react-treebeard'

if (Meteor.isClient)
  require('codemirror/mode/markdown/markdown')

const imageExtensions = ['png', 'jpeg', 'svg']
const treeStyle = {
  tree: {
    base: {
      listStyle: 'none',
      backgroundColor: '#fff6da',
      margin: 0,
      marginTop: '3vh',
      width: '20vw',
      overflow: 'scroll',
      maxHeight: '82vh',
      height: '82vh',
    },
    node: {
      base: {
        position: 'relative'
      },
      link: {
        cursor: 'pointer',
        position: 'relative',
        padding: '0px 5px',
        display: 'block'
      },
      activeLink: {
        background: 'rgba(239, 152, 161, 0.4)',
      },
      toggle: {
        base: {
          position: 'relative',
          display: 'inline-block',
          verticalAlign: 'top',
          marginLeft: '-5px',
          height: '24px',
          width: '24px'
        },
        wrapper: {
          position: 'absolute',
          top: '25%',
          left: '50%',
          margin: '-5px 0 0 -5px',
          height: '10px'
        },
        height: 10,
        width: 10,
        arrow: {
          fill: '#48466d',
        }
      },
      header: {
        base: {
          display: 'inline-block',
          verticalAlign: 'top',
          color: '#48466d'
        },
      },
    }
  }
}

const ProjectDetail = ({ currentUser, history, match: { params: { projectId }}}) => {
  checkIfUserIsNotLogged({ currentUser, history })
  const { results } = useMulti2({
    collectionName: 'projects',
    fragmentName: 'ProjectFragment',
    input: { filter: { _id: { _eq: projectId }}}
  })
  const project = results && results[0]
  const [refresh, setRefresh] = useState(false)
  const [files, setFiles] = getMarkdownFiles(project, refresh)
  const [value, setValue] = useState()
  const [nodeSelected, setNodeSelected] = useState({})
  const [loading, setLoading] = useState(false)

  const updateFileData = (object, path, data) => {
    if (object.path === path) {
      object.updated = true
      return object.data = btoa(data)
    }
  
    if (object.children)
      for (const child of object.children)
        updateFileData(child, path, data)
  }

  const renderImage = () => {

    return (
      <div
        style={{
          display: 'flex',
          justifyContent: 'center',
          width: '100%',
        }}
      >
        <img
          style={{
            height: '85vh',
          }}
          src={`data:image/${nodeSelected.ext};base64,${value}`}
        />
      </div>
    )
  }

  const renderEditor = () => {

    return (
      <CodeMirror
        value={atob(value)}
        options={{
          lineNumbers: true,
          tabSize: 2,
          theme: 'duotone-light',
          mode: 'text/x-markdown',
        }}
        onChange={onChangeFileOnEditor}
      />
    )
  }

  const onToggleFolderOnTree = (node, toggled) => {
    nodeSelected.active = false

    if (node.type === 'file')
      setValue(node.data)

    node.toggled = toggled
    node.active = true
    
    setNodeSelected(node)
    setFiles({...files})
  }

  const toggleLoading = () => {
    setLoading(loading => setLoading(!loading))
  }

  const onChangeFileOnEditor = (editor, data, value) => {
    if (nodeSelected.type === 'file')
      updateFileData(files, nodeSelected.path, value)
  }

  const onPublishWebsite = () => {
    toggleLoading()
    Meteor
    .call(
      'updateFiles',
      ({
        files,
        requirements: project.requirements,
        installation: project.installation,
        projectName: project.name,
      }),
      (error, data) => {
        if (error) return alert(error.message)
        
        alert(data.message)
        toggleLoading()
      }
    )
  }

  const onRefreshWindow = () => {
    toggleLoading()
    setRefresh(refresh => setRefresh(!refresh))
  }

  const onRemoveFolderOrFile = () => {
    const isOk = confirm(`Do you want to delete the ${nodeSelected.type} ${nodeSelected.name}?`)
    if (!isOk) return

    toggleLoading()
    Meteor
    .call(
      'removeFolderOrFile',
      {
        path: nodeSelected.path
      },
      (error, data) => {
        if (error) return alert(error.message)
        
        alert(data.message)
        onRefreshWindow()
      } 
    )
  }

  const onAddFolder = () => {
    const folderName = prompt(`please write the name of the folder that you want to create into ${nodeSelected.name} folder`)
    if (!folderName) return

    toggleLoading()
    Meteor
    .call(
      'addFolder',
      {
        path: `${nodeSelected.path}/${folderName}`
      },
      (error, data) => {
        if (error) return alert(error.message)
        
        alert(data.message)
        onRefreshWindow()
      } 
    )
  }

  const onEditFileName = () => {
    const newName = prompt(`please write the new name of the ${nodeSelected.type} called ${nodeSelected.name}`)
    if (!newName) return

    const nameSplitted = nodeSelected.path.split('\/')
    const target = `${nameSplitted.slice(0, nameSplitted.length - 1).join('/')}/${newName}`

    toggleLoading()
    Meteor
    .call(
      'editNameOfFolderOrFile',
      {
        source: nodeSelected.path,
        target,
      },
      (error, data) => {
        if (error) return alert(error.message)
        
        alert(data.message)
        onRefreshWindow()
      } 
    )
  }

  return (
    <div
      className='bg-secondary'
    >
      <Components.ProjectDetailToolbar
        name={project && project.name}
      />
      <div
        className='d-flex flex-row'
      >
        <div
          className='d-flex flex-column'
        >
          <div
            className='d-flex flex-row position-absolute pl-1'
          >
            <Components.FileChooser
              disabled={nodeSelected.type !== 'folder'}
              path={nodeSelected.path}
              onUpload={toggleLoading}
              onUploaded={onRefreshWindow}
              loading={loading}
            />
            <Components.RemoveFileIcon
              disabled={!nodeSelected.name}
              loading={loading}
              onClick={onRemoveFolderOrFile}
            />
            <Components.EditFileNameIcon
              disabled={!nodeSelected.name}
              loading={loading}
              onClick={onEditFileName}
            />
            <Components.AddFolderIcon
              disabled={nodeSelected.type !== 'folder'}
              loading={loading}
              onClick={onAddFolder}
            />
          </div>
          <Treebeard
            style={treeStyle}
            data={files}
            onToggle={onToggleFolderOnTree}
          />
        </div>
        {
          imageExtensions.indexOf(nodeSelected.ext) !== -1
          ? renderImage()
          : nodeSelected.type === 'file' 
          ? renderEditor()
          : null
        }
      </div>
      <Components.FullButton
        label='Publish'
        onClick={onPublishWebsite}
        loading={loading}
      />
    </div>
  )
}

registerComponent({ name: 'ProjectDetail', component: ProjectDetail, hocs: [withCurrentUser]})
export default ProjectDetail