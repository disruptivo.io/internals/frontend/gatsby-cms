import React from 'react'
import { registerComponent, withCurrentUser, Components } from 'meteor/vulcan:core'
import checkIfUserIsNotLogged from '../../../client/hooks/checkIfUserIsNotLogged';

const Enterprises = ({ currentUser, history }) => {
  checkIfUserIsNotLogged({ currentUser, history })

  const renderSeeProjectsButton = ({ document }) => {

    return (
      <button
        className='btn btn-primary'
        type='button'
        onClick={() => history.push(`/${document._id}/projects`)}
      >
        See
      </button>
    )
  }
  
  return (
    <div
      className="bg-secondary"
    >
      <Components.EnterpriseToolbar />
      <Components.Datatable 
        collectionName='enterprises'
        columns={[
          '_id',
          'name',
          'createdAt',
          'updatedAt',
          'createdBy',
          'updatedBy',
          { name: 'See projects', component: renderSeeProjectsButton }
        ]}
        options={{
          collectionName: 'enterprises',
          fragmentName: 'EnterpriseFragment'
        }}
      />
    </div>
  )
}

registerComponent({ name: 'Enterprises', component: Enterprises, hocs: [withCurrentUser] })
export default Enterprises;