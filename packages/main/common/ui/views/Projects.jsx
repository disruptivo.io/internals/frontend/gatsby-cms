import React from 'react'
import { registerComponent, withCurrentUser, Components } from 'meteor/vulcan:core'
import checkIfUserIsNotLogged from '../../../client/hooks/checkIfUserIsNotLogged'

const Projects = ({ currentUser, history, match: { params: { enterpriseId }}, ...props }) => {
  checkIfUserIsNotLogged({ currentUser, history })
  
  const renderSeeButton = ({ document }) => {

    return (
      <button
        className='btn btn-primary'
        type='button'
        onClick={() => history.push(`/projects/${document._id}`)}
      >
        See
      </button>
    )
  }

  return (
    <div
      className='bg-secondary'
    >
      <Components.ProjectToolbar />
      <Components.Datatable
        collectionName='projects'
        columns={[
          '_id',
          'enterpriseId',
          'name',
          'createdAt',
          'updatedAt',
          'createdBy',
          'updatedBy',
          { name: 'Publish new version', component: renderSeeButton }
        ]}
        options={{
          collectionName: 'projects',
          fragmentName: 'ProjectFragment',
          input: { filter: { enterpriseId: { _eq: enterpriseId }}}
        }}
        newFormOptions={{
          prefilledProps: {
            enterpriseId: enterpriseId,
          },
        }}
      />
    </div>
  )
}

registerComponent({ name: 'Projects', component: Projects, hocs: [withCurrentUser] })
export default Projects;