import React from 'react'
import { registerComponent } from 'meteor/vulcan:core'

const FullButton = ({ label, onClick, loading }) => {

  return (
    <button
      disabled={loading}
      onClick={onClick}
      className='btn btn-primary fullButton vw-100 rounded-0'
      type='button'
    >
      {
        loading &&
        <span
          className='spinner-border spinner-border-sm'
          role='status'
          aria-hidden='true'>
        </span>
      }
      {loading ? 'Loading...' : label}
    </button>
  )
}

registerComponent({ name: 'FullButton', component: FullButton })
export default FullButton