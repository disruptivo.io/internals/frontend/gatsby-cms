import React from 'react'
import { registerComponent } from 'meteor/vulcan:core'

const RemoveFileIcon = ({ onClick, disabled, loading }) => {

  const color = disabled ? '#424242' : '#ef98a1'

  const renderSpinner = () => {
    return (
      <div
        className='spinner-border text-primary mt-2 mr-2'
        style={{
          fontSize: 1,
          width: '0.7rem',
          height: '0.7rem',
        }}
        role='status'>
        <span className='sr-only'>Loading...</span>
      </div>
    )
  }

  const renderIcon = () => {
    return (
      <object
        onClick={disabled ? null : onClick}
        style={{
          cursor: 'pointer',
          marginRight: 5
        }}
      >
        <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-file-earmark-minus" fill={color} xmlns="http://www.w3.org/2000/svg">
          <path d="M9 1H4a2 2 0 0 0-2 2v10a2 2 0 0 0 2 2h5v-1H4a1 1 0 0 1-1-1V3a1 1 0 0 1 1-1h5v2.5A1.5 1.5 0 0 0 10.5 6H13v2h1V6L9 1z"/>
          <path fillRule="evenodd" d="M11 11.5a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1h-4a.5.5 0 0 1-.5-.5z"/>
        </svg>
      </object>
    )
  }

  if (loading)
    return renderSpinner()

  return renderIcon()
}

registerComponent({ name: 'RemoveFileIcon', component: RemoveFileIcon })
export default RemoveFileIcon