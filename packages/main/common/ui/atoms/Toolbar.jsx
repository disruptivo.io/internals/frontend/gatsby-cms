import React from 'react'
import { registerComponent } from 'meteor/vulcan:core'

const Toolbar = ({ left, right, float }) => {

  return (
    <div
      className={`toolbar d-flex flex-row justify-content-between ${float && 'shadow-sm'}`}
    >
      <div
        className='navbar-brand font-weight-bolder d-flex justify-content-center align-items-center ml-3'
      >
        {left}
      </div>      
      <div
        className='navbar-brand text-white d-flex justify-content-center align-items-center mr-3'
      >
        {right}
      </div>      
    </div>
  )
}

registerComponent({ name: 'Toolbar', component: Toolbar })
export default Toolbar