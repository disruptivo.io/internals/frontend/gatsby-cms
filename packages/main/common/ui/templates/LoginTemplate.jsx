import React from 'react'
import { registerComponent } from 'meteor/vulcan:core'

const LoginTemplate = ({ children }) => {
  return (
    <div
      className='d-flex justify-content-center align-items-center bg-secondary vh-100'
    >
      {children}
    </div>
  )
}

registerComponent({ name: 'LoginTemplate', component: LoginTemplate })
export default LoginTemplate;