import { registerFragment } from 'meteor/vulcan:core'

registerFragment(/* GraphQL */`
  fragment ProjectFragment on Project {
    _id
    enterpriseId
    name
    requirements
    installation
    createdAt
    updatedAt
    createdBy {
      displayName
    }
    updatedBy {
      displayName
    }
  }
`)