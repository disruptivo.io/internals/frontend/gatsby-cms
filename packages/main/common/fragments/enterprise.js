import { registerFragment } from 'meteor/vulcan:core'

registerFragment(/* GraphQL */`
  fragment EnterpriseFragment on Enterprise {
    _id
    name
    createdAt
    updatedAt
    createdBy {
      displayName
    }
    updatedBy {
      displayName
    }
  },
`)