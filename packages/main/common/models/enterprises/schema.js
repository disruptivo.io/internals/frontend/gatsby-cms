const onCreateId = () => {
  return uuid.new()
}

const onCreateCreatedAt = () => {
  return new Date()
}

const onUpdateUpdatedAt = () => {
  return new Date()
}

const onResolveCreateBy = (enterprise, args, context) => {
  return context
  .Users
  .findOne(
    {
      _id: context.currentUser._id,
    },
    {
      fields: context
      .Users
      .getReadableProjection(
        context.currentUser,
        context.Users,
      ),
    },
  )
}

const onResolveUpdateBy = (enterprise, args, context) => {
  return context
  .Users
  .findOne(
    {
      _id: context.currentUser._id,
    },
    {
      fields: context
      .Users
      .getReadableProjection(
        context.currentUser,
        context.Users,
      ),
    },
  )
}

export default {
  _id: {
    type: String,
    optional: true,
    canRead: ['admins'],
    onCreate: onCreateId,
  },
  name: {
    type: String,
    optional: false,
    searchable: true,
    canRead: ['admins'],
    canCreate: ['admins'],
    canUpdate: ['admins'],
  },
  createdAt: {
    type: Date,
    optional: true,
    canRead: ['admins'],
    onCreate: onCreateCreatedAt,
  },
  updatedAt: {
    type: Date,
    optional: true,
    canRead: ['admins'],
    onUpdate: onUpdateUpdatedAt,
  },
  _createdBy: {
    type: String,
    optional: true,
    canRead: ['admins'],
    resolveAs: {
      fieldName: 'createdBy',
      type: 'User', 
      resolver: onResolveCreateBy,
    },
  },
  _updatedBy: {
    type: String,
    optional: true,
    canRead: ['admins'],
    resolveAs: {
      fieldName: 'updatedBy',
      type: 'User', 
      resolver: onResolveUpdateBy,
    },
  },
}
