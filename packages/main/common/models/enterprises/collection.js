import { createCollection } from 'meteor/vulcan:core'
import schema from './schema.js'

export default createCollection({
  collectionName: 'enterprises',
  typeName: 'Enterprise',
  schema,
  permissions: {
    canRead: ['admins'],
    canCreate: ['admins'],
    canUpdate: ['admins'],
    canDelete: ['admins'],
  },
  defaultInput: {
    orderBy: {
      createdAt: 'desc',
    },
  },
})
