import { createCollection } from 'meteor/vulcan:core'
import schema from './schema.js'
import path from 'path'
import { execSync } from 'child_process'

const onCreate = async (validationErrors, properties) => {
  try {
    if (validationErrors.length) return validationErrors

    fetchRepository(
      properties.data.repository,
      properties.data.name,
    )
  
    return validationErrors
  } catch (error) {
    console.log(error.message)
    validationErrors.push(error.message)
    return validationErrors
  }
}

const fetchRepository = (repository, name) => {
  const rootPath = execSync('pwd').toString()
  const absoulutePath = path
  .resolve(
    rootPath,
    '../../../../projectsPulled',
  )
  
  execSync(`mkdir -p ${absoulutePath}`)
  execSync(`cd ${absoulutePath} && git clone ${repository} ${name}`)
}

const onDelete = (validationErrors, properties) => {
  try {
    const rootPath = execSync('pwd').toString()
    const absoulutePath = path
    .resolve(
      rootPath,
      '../../../../projectsPulled',
      properties.document.name,
    )
    execSync(`rm -rf ${absoulutePath}`)
  } catch (error) {
    console.log(error.message)
    validationErrors.push(error.message)
    return validationErrors
  }
}

export default createCollection({
  collectionName: 'projects',
  typeName: 'Project',
  schema,
  permissions: {
    canRead: ['admins'],
    canCreate: ['admins'],
    canUpdate: ['admins'],
    canDelete: ['admins'],
  },
  defaultInput: {
    orderBy: {
      createdAt: 'desc',
    },
  },
  callbacks: {
    create: {
      validate: [onCreate],
    },
    delete: {
      validate: [onDelete],
    },
  },
})
