const onCreateId = () => {
  return uuid.new()
}

const onCreateCreatedAt = () => {
  return new Date()
}

const onUpdateUpdatedAt = () => {
  return new Date()
}

const onResolverCreateBy = (project, args, context) => {
  return context
  .Users
  .findOne(
    {
      _id: context.currentUser._id,
    },
    {
      fields: context
      .Users
      .getReadableProjection(
        context.currentUser,
        context.Users
      ), 
    },
  )
}

const onResolverUpdateBy = (project, args, context) => {
  return context
  .Users
  .findOne(
    {
      _id: context.currentUser._id,
    },
    {
      fields: context
      .Users
      .getReadableProjection(
        context.currentUser,
        context.Users,
      ), 
    },
  )
}

export default {
  _id: {
    type: String,
    optional: true,
    canRead: ['admins'],
    onCreate: onCreateId,
  },
  enterpriseId: {
    type: String,
    optional: true,
    canRead: ['admins'],
    canCreate: ['admins'],
  },
  name: {
    type: String,
    optional: false,
    searchable: true,
    canRead: ['admins'],
    canCreate: ['admins'],
    canUpdate: ['admins'],
  },
  repository: {
    type: String,
    optional: false,
    searchable: true,
    canRead: ['admins'],
    canCreate: ['admins'],
    canUpdate: ['admins'],
  },
  requirements: {
    type: String,
    optional: false,
    searchable: true,
    input: 'textarea',
    canRead: ['admins'],
    canCreate: ['admins'],
    canUpdate: ['admins'],
  },
  installation: {
    type: String,
    optional: false,
    searchable: true,
    input: 'textarea',
    canRead: ['admins'],
    canCreate: ['admins'],
    canUpdate: ['admins'],
  },
  createdAt: {
    type: Date,
    optional: true,
    canRead: ['admins'],
    onCreate: onCreateCreatedAt,
  },
  updatedAt: {
    type: Date,
    optional: true,
    canRead: ['admins'],
    onUpdate: onUpdateUpdatedAt,
  },
  _createdBy: {
    type: String,
    optional: true,
    canRead: ['admins'],
    resolveAs: {
      fieldName: 'createdBy',
      type: 'User', 
      resolver: onResolverCreateBy,
    },
  },
  _updatedBy: {
    type: String,
    optional: true,
    canRead: ['admins'],
    resolveAs: {
      fieldName: 'updatedBy',
      type: 'User', 
      resolver: onResolverUpdateBy,
    },
  },
}
