import Users from 'meteor/vulcan:users'
import { Accounts } from 'meteor/accounts-base'

const DEFAULT_USER = {
  username: 'admin',
  email: 'admin@whatever.com',
  password: 'chImIch@ng@125@',
}

Meteor.startup(() => {
  if (Users.find().fetch().length === 0)
    Accounts.createUser(DEFAULT_USER)
})