import fs from 'fs'

Meteor.methods({
  uploadFile({ path, fileName, file }) {
    try {
      const absolutePath = `${path}/${fileName}`
      fs.writeFileSync(
        absolutePath,
        new Buffer(file, 'binary')
      )
      
      return {
        code: 0,
        message: 'Success'
      }
    } catch (error) {
      return {
        code: 500,
        message: error.message
      }
    }
  }
})