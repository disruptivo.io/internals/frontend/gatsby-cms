import { execSync } from 'child_process'

Meteor.methods({
  removeFolderOrFile({ path }) {
    try {
      execSync(`rm -rf "${path}"`)
      
      return {
        code: 0,
        message: 'Success'
      }
    } catch (error) {
      return {
        code: 500,
        message: error.message
      }
    }
  }
})