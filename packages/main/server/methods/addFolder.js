import { execSync } from 'child_process'

Meteor.methods({
  addFolder({ path }) {
    try {
      execSync(`mkdir -p ${path}`)
      
      return {
        code: 0,
        message: 'Success'
      }
    } catch (error) {
      return {
        code: 500,
        message: error.message
      }
    }
  }
})