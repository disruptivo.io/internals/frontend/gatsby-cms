import fs from 'fs'
import path from 'path'
import { exec } from 'child_process'

const updateFilesUpdated = (object) => {
  if (object.updated)
    updateContentOfFile(object)

  if (object.children)
    for (const child of object.children)
      updateFilesUpdated(child)
}

const updateContentOfFile = (object) => {
  fs.truncateSync(object.path, 0)
  fs.writeFileSync(object.path, Buffer.from(object.data, 'base64').toString())
}

const installRequirements = async (requirements) => {
  requirements = JSON.parse(unescape(requirements))
  const requirementNames = Object
  .keys(requirements)
  
  if (requirementNames.length)
    await asyncForEach(requirementNames, async (requirementName) => {
      const whereis = await executeCommand(`whereis ${requirementName}`)
      if (whereis.length == requirementName.length + 2)
        await installRequirement(requirements[requirementName])
    })
}

const installRequirement = async (requirement) => {
  await asyncForEach(
    requirement.commandsForInstallation,
    async (command) => {
      await executeCommand(command)
    }
  )
}

const executeInstallation = async ({ projectName, installation }) => {
  const commands = unescape(installation).split(',')
  const projectPath = path
  .resolve(
    await executeCommand('pwd'),
    `../../../../projectsPulled/${projectName}/`,
  )
  
  await asyncForEach(commands, async (command) => {
    command = `cd ${projectPath} && ${command}`
    await executeCommand(command)
  })
}

const executeCommand = (command) => {
  console.log(`Executing command: ${command} >>>>>>>>`)

  return new Promise((resolve, reject) => {
    exec(command, (error, stdout) => {
      console.log(`>>>>>>>> stdout`);
      console.log(stdout);
      console.log(`<<<<<<<< stdout`);
      if (error)
        return reject({ code: error.code, message: error.message })

      resolve(stdout)
    })
  })
}

const asyncForEach = async (array, callback) => {
  for (let index = 0; index < array.length; index++) {
    await callback(
      array[index], index, array
    )
  }
}

Meteor.methods({
  async updateFiles({ files, requirements, installation, projectName }) {
    try {
      updateFilesUpdated(files)
      await installRequirements(requirements)
      await executeInstallation({ projectName, installation })
      
      return {
        code: 0,
        message: 'Success',
      }
    } catch (error) {
      console.log(`Error: ${error}`)
      return {
        code: 500,
        message: error.message,
      }
    }
  }
})