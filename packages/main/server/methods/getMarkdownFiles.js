import path from 'path'
import fs from 'fs'
import { execSync } from 'child_process'

const getFiles = (absolutePath) => {
  const stats = fs.lstatSync(absolutePath)
  
  const info = {
      type: 'folder',
      path: absolutePath,
      name: path.basename(absolutePath),
  }

  if (stats.isDirectory()) {
    info.toggled = true
    info.children = fs
    .readdirSync(absolutePath)
    .map((child) => getFiles(absolutePath + '/' + child))
  } else {
    info.ext = absolutePath.split('.').pop()
    info.type = 'file'
    info.data = fs.readFileSync(absolutePath, { encoding: 'base64' })
  }

  return info
}

Meteor.methods({
  getMarkdownFiles(projectName) {
    const relativePath = path
    .resolve(
      execSync('pwd').toString(),
      `../../../../projectsPulled/${projectName}/assets`,
    )
    const files = getFiles(relativePath)

    return files
  }
})