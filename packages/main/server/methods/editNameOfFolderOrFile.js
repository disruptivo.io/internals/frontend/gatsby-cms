import { execSync } from 'child_process'

Meteor.methods({
  editNameOfFolderOrFile({ source, target }) {
    try {
      execSync(`mv  "${source}" "${target}"`)
      
      return {
        code: 0,
        message: 'Success'
      }
    } catch (error) {
      return {
        code: 500,
        message: error.message
      }
    }
  }
})