#!/bin/bash
echo "Please introduce your ROOT_URL: "
read ROOT_URL
echo "Please introduce your MONGO_URL: "
read MONGO_URL

export METEOR_PACKAGE_DIRS=./packages/*
export METEOR_SETTINGS="$(cat settings.json)"
export PORT=3000
export ROOT_URL=$ROOT_URL
export MONGO_URL=$MONGO_URL

npm run build
node ./bundle/main.js