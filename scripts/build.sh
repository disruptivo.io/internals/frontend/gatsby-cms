#!/bin/bash
npm i --production
export NODE_ENV=production
export METEOR_PACKAGE_DIRS=./packages/*
meteor build .
tar xvzf gatsby-cms.tar.gz
(cd ./bundle/programs/server && npm install)