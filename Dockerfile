FROM python:3

WORKDIR /gatsby-cms

RUN apt-get update
RUN apt-get install -y curl
RUN apt-get install -y git
RUN apt-get install -y nano vim

RUN mkdir -p /usr/bin/nvm
ENV NVM_DIR /usr/bin/nvm
ENV NODE_VERSION v12.16.1

RUN mkdir -p /root/.ssh
ADD id_rsa /root/.ssh/id_rsa
RUN chmod 700 /root/.ssh/id_rsa
RUN echo "Host gitlab.com\n\tStrictHostKeyChecking no\n" >> /root/.ssh/config

RUN curl https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash \
    && . $NVM_DIR/nvm.sh \
    && nvm install $NODE_VERSION \
    && nvm alias default $NODE_VERSION \
    && nvm use default
RUN curl https://install.meteor.com/ | bash

ENV NODE_PATH $NVM_DIR/versions/node/$NODE_VERSION/lib/node_modules
ENV PATH $NVM_DIR/versions/node/$NODE_VERSION/bin:$PATH
ENV METEOR_ALLOW_SUPERUSER true

COPY . .
RUN npm i

EXPOSE 3000
CMD [ "npm", "run", "start:dev" ]