# How to run?
## Development mode
### Normal way

1. install nvm, instructions [here](https://github.com/nvm-sh/nvm#installing-and-updating)
2. install Meteor, instructions [here](https://www.meteor.com/install)
3. download the code
    ```bash
    git clone git@gitlab.com:disruptivo.io/internals/frontend/gatsby-cms.git
    ```
4. install dependencies
    ```bash
    cd gatsby-cms && nvm install && nvm use && npm i
    ```
5. create a file called `setting.json` taking as reference the `settings.example.json`
6. run the app
    ```bash
    npm run start:dev
    ```

### Using docker

1. download the code
    ```bash
    git clone git@gitlab.com:disruptivo.io/internals/frontend/gatsby-cms.git
    ```
2. go to the root folder
    ```bash
    cd gatsby-cms
    ```
3. create a file called `setting.json` taking as reference the `settings.example.json`
4. create and ssh key, push the public in your repo and the private in the root folder into a file called `id_rsa`
5. create the image
    ```bash
    docker build --pull --rm -f "Dockerfile" -t gatsby-cms:1.0.0 "."
    ```
6. run the container using the image created
    ```bash
    docker run -d -p 3000:3000 gatsby-cms:1.0.0
    ```

## Production mode
### Using the bundle

1. download the code
    ```bash
    git clone git@gitlab.com:disruptivo.io/internals/frontend/gatsby-cms.git
    ```
2. install dependencies
    ```bash
    cd gatsby-cms && nvm install && nvm use && npm i
    ```
3. create a file called `setting.json` taking as reference the `settings.example.json`
4. run the project with the following command:
    ```bash
    npm run start:prd
    ```
5. the previous command will request you the following data:
   1. ROOT_URL: http://yourdomain.com
   2. MONGO_URL: mongodb+srv://user:password@mongo.domain/database

# How to use?

1. login using the following user
    ```javascript
    {
      username: 'admin',
      email: 'admin@whatever.com',
      password: 'chImIch@ng@125@',
    }
    ```
2. create an entreprise
3. create a project using the following example
    ```
    repository: YOUR_REPO_URL

    requiremets: {
      "zip": {
        "commandsForInstallation": [
          "apt-get install -y zip unzip"
        ]
      },
      "ncftp": {
        "commandsForInstallation": [
          "apt-get install -y ncftp"
        ]
      },
      "curl": {
        "commandsForInstallation": [
          "apt-get install -y curl"
        ]
      }
    }

    installation:
    . $NVM_DIR/nvm.sh && nvm install,
    . $NVM_DIR/nvm.sh && nvm install,
    . $NVM_DIR/nvm.sh && nvm use && export NODE_ENV=development && npm i,
    . $NVM_DIR/nvm.sh && nvm use && npm run clean,
    . $NVM_DIR/nvm.sh && nvm use && export NODE_ENV=production && npm run build,
    cd ./public && zip -r ../FILE_NAME.zip .,
    ncftpput -u FTP_USER -p FTP_PASSWORD -P 21 -R FTP_DOMAIN / ./extractor.php,
    ncftpput -u FTP_USER -p FTP_PASSWORD -P 21 -R FTP_DOMAIN / ./FILE_NAME.zip,
    curl http://FTP_DOMAIN/extractor.php,
    git add .,
    git commit -m "feature: content updated",
    git push origin master,
    rm -rf ./node_modules package-lock.json FILE_NAME.zip
    ```
4. modify the content and press publish button

# Hints

- if you cannot upload the bundle to your VPS you can push it to Google Drive and use the following script for downloading the bundle to your VPS
    ```sh
    fileId=FILE_ID (e.g. 13RlxtiN3LYhMBSuom6y7xWfE8pkgaRtu)
    fileName=FILE_NAME (e.g. bundle.zip)
    curl -sc /tmp/cookie "https://drive.google.com/uc?export=download&id=${fileId}" > /dev/null
    code="$(awk '/_warning_/ {print $NF}' /tmp/cookie)"  
    curl -Lb /tmp/cookie "https://drive.google.com/uc?export=download&confirm=${code}&id=${fileId}" -o ${fileName}
    ```
- you can run the app using [pm2](https://pm2.keymetrics.io/)
    ```sh
    METEOR_SETTINGS="$(cat settings.json)" PORT=YOUR_PORT ROOT_URL=YOUR_ROOT_URL MONGO_URL=YOUR_MONGO_URL pm2 start main.js --name APP_NAME
    ```